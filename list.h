#ifndef LIST_H
#define LIST_H

#include <pthread.h>

typedef struct esw_node {
    char * key;
    char * address;
    struct esw_node * next;
} esw_node_t;

typedef struct esw_list {
    esw_node_t * head;
    pthread_mutex_t lock;
} esw_list_t;

void esw_list_init(esw_list_t * list);
void esw_list_push(esw_list_t * list, const char * const key, const char * const address);
void esw_list_append(esw_list_t * list, const char * const key, const char * const address);
void esw_list_remove(esw_list_t * list, const char * const key);
void esw_list_update(esw_list_t * list, const char * const key, const char * const address);
int esw_list_find(esw_list_t * list, const char * const key, char * address, const int max_len);
esw_node_t * esw_list_create_node(const char * const key, const char * const address);
void esw_list_free_node(esw_node_t * node);
void esw_list_free_content(esw_list_t * list);
void esw_list_free(esw_list_t * list);
void esw_list_print(esw_list_t * list);
void esw_list_node_print(esw_node_t * list);

#endif // LIST_H
